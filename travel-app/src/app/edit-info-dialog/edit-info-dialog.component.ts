import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../app.service';

@Component({
  selector: 'app-edit-info-dialog',
  templateUrl: './edit-info-dialog.component.html',
  styleUrls: ['./edit-info-dialog.component.css'],
})
export class EditInfoDialogComponent implements OnInit {
  recordHotelForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl(''),
    phoneNumber: new FormControl(''),
    website: new FormControl(''),
    id: new FormControl(1),
  });
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService
  ) {}

  ngOnInit(): void {
    this.setData();
  }

  setData(): void {
    this.recordHotelForm.get('name')?.setValue(this.data.name);
    this.recordHotelForm.get('email')?.setValue(this.data.email);
    this.recordHotelForm.get('phoneNumber')?.setValue(this.data.phoneNumber);
    this.recordHotelForm.get('website')?.setValue(this.data.website);
    this.recordHotelForm.get('address')?.setValue(this.data.address);
    this.recordHotelForm.get('id')?.setValue(this.data.id);
  }

  updateRecord(): void {
    const id = this.recordHotelForm.get('id')?.value ?? 1;
    const newData = this.recordHotelForm.value;
    console.log('new', newData);
    this.apiService.updateData(id, newData).subscribe(
      (res) => {
        console.log('delete successfully', res);
      },
      (error) => {
        console.log('delete error', error);
      }
    );
  }

  addNewRecord(): void {
    const newData = this.recordHotelForm.value;
    this.apiService.addData(newData).subscribe(
      (res) => {
        console.log('delete successfully', res);
      },
      (error) => {
        console.log('delete error', error);
      }
    );
  }
}
