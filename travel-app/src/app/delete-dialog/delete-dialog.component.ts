import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../app.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css'],
})
export class DeleteDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService
  ) {}

  deleteRecord(): void {
    const id = this.data.id;
    this.apiService.deleteData(id).subscribe(
      (res) => {
        console.log('delete successfully', res);
      },
      (error) => {
        console.log('delete error', error);
      }
    );
  }
}
