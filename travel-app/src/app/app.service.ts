import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl =
    'https://e2d79337-7d71-445e-8fcf-88c289255c68.mock.pstmn.io/getHotel';

  constructor(private http: HttpClient) {}

  getData(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }

  deleteData(id: number): Observable<any> {
    const deleteUrl = `${this.apiUrl}/${id}`;
    return this.http.delete(deleteUrl);
  }

  updateData(id: number, data: any): Observable<any> {
    const updateUrl = `${this.apiUrl}/${id}`;

    return this.http.put(updateUrl, data);
  }

  addData(data: any): Observable<any> {
    return this.http.put(this.apiUrl, data);
  }
}
