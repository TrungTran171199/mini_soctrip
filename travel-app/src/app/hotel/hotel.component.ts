import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { filter, first, Observable } from 'rxjs';
import { AddNewRecordComponent } from '../add-new-record/add-new-record.component';
import { ApiService } from '../app.service';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { EditInfoDialogComponent } from '../edit-info-dialog/edit-info-dialog.component';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css'],
})
export class HotelComponent implements OnInit {
  constructor(private apiService: ApiService, public dialog: MatDialog) {}
  hotelInfo = [];

  ngOnInit(): void {
    this.apiService.getData().subscribe((res) => {
      this.hotelInfo = res.hotelInfo;
    });
  }

  openDialog(data: any): void {
    const dialogRef = this.dialog.open(EditInfoDialogComponent, { data });
    dialogRef.afterClosed().subscribe((res) => {
      console.log('response', res);
    });
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddNewRecordComponent);
    dialogRef.afterClosed().subscribe((res) => {
      console.log('res', res);
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.afterClosed().subscribe((res) => {
      console.log('res', res);
    });
  }
}
