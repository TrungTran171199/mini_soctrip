import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../app.service';

@Component({
  selector: 'app-add-new-record',
  templateUrl: './add-new-record.component.html',
  styleUrls: ['./add-new-record.component.css'],
})
export class AddNewRecordComponent {
  recordHotelForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl(''),
    phoneNumber: new FormControl(''),
    website: new FormControl(''),
    id: new FormControl(2),
  });
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService
  ) {}

  addNewRecord(): void {
    const newData = this.recordHotelForm.value;
    console.log('new', newData);
    this.apiService.addData(newData).subscribe(
      (res) => {
        console.log('delete successfully', res);
      },
      (error) => {
        console.log('delete error', error);
      }
    );
  }
}
